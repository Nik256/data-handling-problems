package service;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ProblemService {
    public BigDecimal calculateAreaOfCircle(double radiusOfCircle) {
        BigDecimal areaOfCircle = new BigDecimal(radiusOfCircle);
        areaOfCircle = areaOfCircle.pow(2).multiply(new BigDecimal(Math.PI));
        return areaOfCircle.setScale(50, RoundingMode.HALF_DOWN);
    }

    public boolean isThirdNumberTheSumOfFirstAndSecondNumbers(String strNumber1, String strNumber2, String strNumber3) {
        BigDecimal number1 = new BigDecimal(strNumber1);
        BigDecimal number2 = new BigDecimal(strNumber2);
        BigDecimal number3 = new BigDecimal(strNumber3);
        return number3.compareTo(number1.add(number2)) == 0;
    }

    public void showMinAndMax(int number1, int number2, int number3) {
        int min = Math.min(Math.min(number1, number2), number3);
        int max = Math.max(Math.max(number1, number2), number3);
        System.out.println("Min = " + min + " / Max = " + max);
    }
}
