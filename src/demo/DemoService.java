package demo;

import service.ProblemService;

public class DemoService {
    private ProblemService problemService;

    public DemoService(ProblemService problemService) {
        this.problemService = problemService;
    }

    public void execute() {
        double radius = 10.23;
        System.out.println("r = " + radius+ " // S = Pi*(r^2) = " + problemService.calculateAreaOfCircle(radius));

        String strNumber1 = "0.1";
        String strNumber2 = "0.15";
        String strNumber3 = "0.25";
        System.out.println(strNumber1 + " + " + strNumber2 + " = " + strNumber3 + "? " +
                problemService.isThirdNumberTheSumOfFirstAndSecondNumbers(strNumber1, strNumber2, strNumber3));

        int number1 = 123;
        int number2 = 456;
        int number3 = 321;
        System.out.println("Numbers: (" + number1 + ", " + number2 + ", " + number3 + ")");
        problemService.showMinAndMax(number1, number2, number3);

    }
}
